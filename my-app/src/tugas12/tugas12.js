import React, {Component} from 'react'

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time: 100
    }
	
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
	setInterval(this.showTime, 500);
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: this.state.time - 1 
    });
  }
  
   checkTime(i) {
		if (i < 10) {
			i = "0" + i;
		}
		//return i;
		this.setState({
		  i: "0" +1
		});
	}
	
	showTime() {
		var a_p = "";
		var today = new Date();
		var curr_hour = today.getHours();
		var curr_minute = today.getMinutes();
		var curr_second = today.getSeconds();
		if (curr_hour < 12) {
			a_p = "AM";
		} else {
			a_p = "PM";
		}
		if (curr_hour == 0) {
			curr_hour = 12;
		}
		if (curr_hour > 12) {
			curr_hour = curr_hour - 12;
		}
		var curr_hour = this.checkTime(curr_hour);
		var curr_minute = this.checkTime(curr_minute);
		var curr_second = this.checkTime(curr_second);
		var pukul = curr_hour + ":" + curr_minute + ":" + curr_second + " " + a_p;
	}

  render(){
    return(
      <>
        <h1 style={{textAlign: "center"}}>
		Sekarang Jam : {this.state.jam}
          Hitung mundur: {this.state.time}
        </h1>
      </>
    )
  }
}

export default Timer