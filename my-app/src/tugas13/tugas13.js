class Lists extends Component{

  constructor(props){
    super(props)
    this.state ={
		hargaBuah : [
			{'Semangka', 'Anggur', 'Strawberry', 'Jeruk','Mangga'},
			{10000, 40000, 30000, 30000, 30000 },
			{'1kg', '0.5kg', '0.4kg', '1kg', '0.5kg'}
		]      
    }
  }

  render(){
    return(
      <>
        <h1>Daftar Peserta Lomba</h1>
        <table>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
			  <th>Harga</th>
			  <th>Berat</th>
            </tr>
          </thead>
          <tbody>
              {
                this.state.pesertaLomba.map((val, index)=>{
                  return(                    
                    <tr>
                      <td>{index+1}</td>
                      <td>{val[0]}</td>
					  <td>{val[1]}</td>
					  <td>{val[2]}</td>
                    </tr>
                  )
                })
              }
          </tbody>
        </table>
      </>
    )
  }
}